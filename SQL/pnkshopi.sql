-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2022 at 08:54 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `ac_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(512) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `email` text NOT NULL,
  `points` float NOT NULL DEFAULT 0,
  `sid` varchar(256) NOT NULL,
  `role` int(3) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`ac_id`, `username`, `password`, `salt`, `email`, `points`, `sid`, `role`) VALUES
(33, 'hyperstudio', '5ad01ccb4a05e40ce62da75e407b23d9', 'f36038f0566c67c9', 'hyperstudio@hyper.page', 0, 'NjkxMzViNzg1YTRlMTEzMjE4aHlwZXJzdHVkaW8=', 779);

-- --------------------------------------------------------

--
-- Table structure for table `card_image`
--

CREATE TABLE `card_image` (
  `image_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `image_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `data_selled`
--

CREATE TABLE `data_selled` (
  `selled_id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `ac_id` int(11) NOT NULL,
  `selled_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gaitun_stock`
--

CREATE TABLE `gaitun_stock` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `owner` varchar(32) DEFAULT '',
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `game_card`
--

CREATE TABLE `game_card` (
  `card_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `card_title` text NOT NULL,
  `card_price` double NOT NULL,
  `card_detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `game_data`
--

CREATE TABLE `game_data` (
  `data_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `detail` text NOT NULL,
  `selled` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `game_type`
--

CREATE TABLE `game_type` (
  `game_id` int(11) NOT NULL,
  `game_name` text NOT NULL,
  `game_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `game_type`
--

INSERT INTO `game_type` (`game_id`, `game_name`, `game_image`) VALUES
(10, 'free fire', '7acc13f95f2b27a248a71797b99cc9a6_game.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `history_pay`
--

CREATE TABLE `history_pay` (
  `pay_id` int(11) NOT NULL,
  `username` text NOT NULL,
  `link` text NOT NULL,
  `amount` float NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history_pay`
--

INSERT INTO `history_pay` (`pay_id`, `username`, `link`, `amount`, `date`) VALUES
(3, 'hyperstudio', 'y7gMecjIKprVJemngf', 10, '2021-02-25 11:55:01');

-- --------------------------------------------------------

--
-- Table structure for table `history_reward`
--

CREATE TABLE `history_reward` (
  `id` int(11) NOT NULL,
  `reward_type` varchar(255) NOT NULL,
  `reward_info` varchar(255) NOT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `reward_point` float NOT NULL DEFAULT 0,
  `ac_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `image_slide`
--

CREATE TABLE `image_slide` (
  `slide_id` int(11) NOT NULL,
  `image_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image_slide`
--

INSERT INTO `image_slide` (`slide_id`, `image_name`) VALUES
(1, 'bannerani.png');

-- --------------------------------------------------------

--
-- Table structure for table `setting_slices`
--

CREATE TABLE `setting_slices` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `reward_type` varchar(255) NOT NULL,
  `reward_point` float NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting_slices`
--

INSERT INTO `setting_slices` (`id`, `image`, `message`, `reward_type`, `reward_point`, `percent`) VALUES
(1, '/assets/img/Spin/1.png', '1', 'item', 10, 0),
(2, '/assets/img/Spin/1.png', '1', 'item', 1, 0),
(3, '/assets/img/Spin/3.png', '3', 'item', 20, 2),
(4, '/assets/img/Spin/4.png', 'รหัสไก่ตัน', 'account', 0, 100),
(5, '/assets/img/Spin/5.png', '5', 'item', 0, 0),
(6, '/assets/img/Spin/6.png', '6', 'item', 0, 0),
(7, '/assets/img/Spin/7.png', '7', 'point', 5, 0),
(8, '/assets/img/Spin/8.png', '8', 'point', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_config`
--

CREATE TABLE `web_config` (
  `con_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `facebook` text NOT NULL,
  `usepoint` float NOT NULL,
  `detail` text NOT NULL,
  `image` text NOT NULL,
  `opened` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_config`
--

INSERT INTO `web_config` (`con_id`, `name`, `facebook`, `usepoint`, `detail`, `image`, `opened`) VALUES
(1, 'hyperstudio', 'pagehyperstudio', 10, 'Hello hyperstudio!', '6658fa810d7cc8d3efe2ff9eb7557f2d_logo.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`ac_id`);

--
-- Indexes for table `card_image`
--
ALTER TABLE `card_image`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `data_selled`
--
ALTER TABLE `data_selled`
  ADD PRIMARY KEY (`selled_id`);

--
-- Indexes for table `gaitun_stock`
--
ALTER TABLE `gaitun_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_card`
--
ALTER TABLE `game_card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `game_data`
--
ALTER TABLE `game_data`
  ADD PRIMARY KEY (`data_id`);

--
-- Indexes for table `game_type`
--
ALTER TABLE `game_type`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `history_pay`
--
ALTER TABLE `history_pay`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `history_reward`
--
ALTER TABLE `history_reward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_slide`
--
ALTER TABLE `image_slide`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `setting_slices`
--
ALTER TABLE `setting_slices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_config`
--
ALTER TABLE `web_config`
  ADD PRIMARY KEY (`con_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `ac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `card_image`
--
ALTER TABLE `card_image`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `data_selled`
--
ALTER TABLE `data_selled`
  MODIFY `selled_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `gaitun_stock`
--
ALTER TABLE `gaitun_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game_card`
--
ALTER TABLE `game_card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `game_data`
--
ALTER TABLE `game_data`
  MODIFY `data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `game_type`
--
ALTER TABLE `game_type`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `history_pay`
--
ALTER TABLE `history_pay`
  MODIFY `pay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `history_reward`
--
ALTER TABLE `history_reward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image_slide`
--
ALTER TABLE `image_slide`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `setting_slices`
--
ALTER TABLE `setting_slices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `web_config`
--
ALTER TABLE `web_config`
  MODIFY `con_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
