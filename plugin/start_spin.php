<?php
session_start();
include("hyper_api.php");
$errorMSG = "";

if (isset($_POST['start_spin'])) {
    $web_con = $hyper->connect->query("SELECT * FROM web_config WHERE con_id = 1")->fetch_array();

    $sid = $_SESSION['USER_SID'];
    $data_user = $hyper->connect->query("SELECT * FROM accounts WHERE sid = '" . $sid . "' ")->fetch_array();

    $getdataslice_wheel = $hyper->connect->query('SELECT * FROM setting_slices');

    $usepoint = $web_con['usepoint'];
    $user_point = $data_user['points'];
    $ac_id = $data_user['ac_id'];
    $getdataslice_wheel_results = mysqli_fetch_assoc($getdataslice_wheel);

    if ($user_point >= $usepoint) {
        while ($key = mysqli_fetch_assoc($getdataslice_wheel)) {
            $random[$key['id']] = $key['percent'];
        }
        $newrandom = array();
        foreach ($random as $fruit => $value) {
            $newrandom = array_merge($newrandom, array_fill(0, $value, $fruit));
        }
        $myrandom = $newrandom[array_rand($newrandom)];
        $total_point_spin_fisrt = $user_point - $usepoint;
        $update_point_user = $hyper->connect->query("UPDATE accounts SET points = $total_point_spin_fisrt WHERE ac_id = $ac_id");
        if ($update_point_user) {
            $new_update_user_point_after_click = $hyper->connect->query("SELECT * FROM accounts WHERE sid = '" . $sid . "' ")->fetch_array();
            $dataslicerandom = $hyper->connect->query("SELECT * FROM setting_slices WHERE id = $myrandom")->fetch_array();
            $rewardpointhistory = 0;
            $detail = "";
            if ($dataslicerandom['reward_type'] == "point") {
                $rewardpointhistory = $dataslicerandom['reward_point'];
                $point_reward_total = $new_update_user_point_after_click['points'] + $dataslicerandom['reward_point'];
                $updatepointuser = $hyper->connect->query("UPDATE accounts SET points = '" . $point_reward_total . "' WHERE ac_id = $ac_id");
            }
            if ($dataslicerandom['reward_type'] == "account") {
                $check_stock = $hyper->connect->query('SELECT count(*) FROM gaitun_stock WHERE owner = ""')->fetch_array();
                if ($check_stock[0] > 0) {
                    $select_stock = $hyper->connect->query('SELECT * FROM gaitun_stock WHERE owner= "" ORDER BY RAND() LIMIT 1');
                    $stock_result = mysqli_fetch_assoc($select_stock);
                    $set_owner = $hyper->connect->query("UPDATE gaitun_stock SET owner = '".$new_update_user_point_after_click['username']."', date= '".date("Y-m-d H:i:s")."' WHERE id = '".$stock_result['id']."'");
                    $detail = $stock_result['content'];
                } else {
                    $total = $new_update_user_point_after_click['points'] + $usepoint;
                    $update_point_user = $hyper->connect->query("UPDATE accounts SET points = '" . $total . "' WHERE ac_id = $ac_id");
                    $errorMSG = 'รหัสหมด';
                }
            }
            $addtohistory = $hyper->connect->query("INSERT INTO history_reward 
            (reward_type, reward_info, detail, reward_point, ac_id) 
            VALUES ('" . $dataslicerandom['reward_type'] . "', 
            '" . $dataslicerandom['message'] . "', 
            '".$detail."', 
            '$rewardpointhistory', 
            '$ac_id')");
            $new_user = $hyper->connect->query("SELECT * FROM accounts WHERE sid = '" . $sid . "' ")->fetch_array();
        }
    } else {
        $errorMSG = 'ยอดเงินไม่เพียงพอ';
    }


    /* result */
    header('content-type: application/json');
    if (empty($errorMSG)) {
        echo json_encode(['code' => 200, 'spin_value' => $myrandom, 'pointnow' => $new_user['points']]);
    } else {
        http_response_code(400);
        echo json_encode(['code' => 400, 'msg' => $errorMSG]);
    }
} else {
    header("Location: 403.php");
}
