<?php

include("hyper_api.php");
$errorMSG = "";

if(isset($_POST['id'])){
   
    if($hyper->user->AuthAdminPermission() === true){
        extract($_POST);
        if (empty($id) || empty($image) || empty($infos) || empty($reward_type)) {
			$errorMSG = 'กรุณากรอกข้อมูลให้ครบถ้วน';
		}else{	
            $update_wheel = $hyper->connect->query("UPDATE setting_slices 
            SET image='".$image."',
            message='".$infos."',
            reward_type='".$reward_type."',
            reward_point='".$point."',
            percent='".$percent."' 
            WHERE id = $id");
		}
    }else{ $errorMSG =  $hyper->user->AuthAdminPermission(); }
    
    /* result */
    header("content-type: application/json");
    if(empty($errorMSG)){
        http_response_code(200);
        echo json_encode(['code'=>200,]);
    }else{
        http_response_code(400);
        echo json_encode(['code'=>400, 'msg'=>$errorMSG]);
    }

}else{
  header("Location: 403.php");
}

?>