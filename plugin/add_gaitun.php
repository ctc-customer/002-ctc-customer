<?php

include("hyper_api.php");
$errorMSG = "";

if(isset($_POST['data'])){
   
    if($hyper->user->AuthAdminPermission() === true){
        extract($_POST);
        if(empty($data)){
            $errorMSG = 'กรุณากรอกข้อมูล';
        }else{
            $req_data = $data;
		    $allData = preg_split('/\r\n|\r|\n/', $req_data);
            if (array_values($allData)[0] == '<batch>') {
                foreach ($allData as $myData) {
                    if ($myData != '<batch>') {
                        $q1 = $hyper->connect->query('INSERT INTO gaitun_stock (content, owner) VALUES ("'.$myData.'","")');
                    }
                }
            }else{
                $q1 = $hyper->connect->query('INSERT INTO gaitun_stock (content, owner) VALUES ("'.$req_data.'","")');
            }
        }
    }else{ $errorMSG =  $hyper->user->AuthAdminPermission(); }
    
    /* result */
    header("content-type: application/json");
    if(empty($errorMSG)){
        http_response_code(200);
        echo json_encode(['code'=>200,]);
    }else{
        http_response_code(400);
        echo json_encode(['code'=>400, 'msg'=>$errorMSG]);
    }

}else{
  header("Location: 403.php");
}
