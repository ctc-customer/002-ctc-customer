      <!-- Web Config -->
      <div class="card mt-4 shadow-dark radius-border hyper-bg-white ml-auto mr-auto">
        <div class="card-body">
          <h4 class="mt-0 mb-4 text-center">ตั้งค่าเว็บไซต์</h4>

          <div class="row no-gutters">

            <div class="col-12 col-lg-5">
              <form method="POST" enctype="multipart/form-data">
                <?php
                $sql_select_web = "SELECT * FROM web_config WHERE con_id = 1";
                $query_web = $hyper->connect->query($sql_select_web);
                $web = mysqli_fetch_array($query_web);
                ?>
                <!-- Card Example -->
                <div class="media m-auto">
                  <img id="gamelogoimgnew" src="<?= $url ?>assets/img/<?= $web['image']; ?>" class="align-self-center mr-3 rounded-circle d-none d-md-block" width="70px;" height="70px;">
                  <div class="media-body text-center text-md-left">
                    <img id="gamelogoresimgnew" src="<?= $url ?>assets/img/<?= $web['image']; ?>" class="ml-auto mr-auto rounded-circle d-block d-md-none" width="70px;" height="70px;">
                    <h4 class="mt-0 mb-1" id="gamenamenew"><?= $web['name']; ?></h4>
                    <font class="text-muted">แนะนำขนาด 150 x 150 Pixel</font>
                  </div>
                </div>
                <!-- End Card Example -->

                <input type="file" style="display:none;" id="logo" onchange="gamelogoURL(this,'new');" accept=".jpg,.png" />
                <button onclick="uploadgamelogo('')" type="button" class="btn btn-sm hyper-btn-info w-100 mt-3"><i class="fal fa-image mr-1"></i>เปลี่ยนรูปภาพ</button>

                <div class="input-group input-group-sm mb-3 mt-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text hyper-bg-dark border-dark">ชื่อเว็บไซต์</span>
                  </div>
                  <input id="name" value="<?= $web['name']; ?>" type="text" onkeyup="txtgamepreview(this,'new')" maxlength="32" class="form-control form-control-sm hyper-form-control" placeholder="ชื่อเว็บไซต์" required>
                </div>

                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text hyper-bg-dark border-dark">Facebook</span>
                  </div>
                  <input id="facebook" value="<?= $web['facebook']; ?>" type="text" class="form-control form-control-sm hyper-form-control" placeholder="Facebook" required>
                </div>


                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <label class="input-group-text hyper-bg-dark border-dark" for="inputGroupSelect01">สถานะเว็บไซต์</label>
                  </div>
                  <select id="open" class="custom-select hyper-form-control" id="inputGroupSelect01">
                    <option <?php if ($web['opened'] == 1) {
                              echo 'selected';
                            } ?> value="1">เปิดให้บริการ</option>
                    <option <?php if ($web['opened'] == 999) {
                              echo 'selected';
                            } ?> value="999">ปิดปรับปรุงชั่วคราว</option>
                  </select>
                </div>

                <div class="input-group input-group-sm mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text hyper-bg-dark border-dark">รายละเอียด</span>
                  </div>
                  <textarea id="detail" class="form-control form-control-sm hyper-form-control" style="height: 100px;min-height: 100px;max-height: 100px;"><?= $web['detail']; ?></textarea>
                </div>

                <button type="submit" id="updatedata<?= $web['con_id']; ?>" class="d-none"></button>

                <button onclick="updatedata('<?= $web['con_id']; ?>')" class="btn btn-sm hyper-btn-notoutline-success my-2 my-sm-0 mr-2 w-100" type="button"><i class="fal fa-check-circle mr-1"></i> อัพเดทข้อมูล</button>
              </form>
            </div>

            <div class="col-12 col-lg-7 pl-lg-4 pt-3">

              <!-- Image Banner -->
              <div id="carouselExampleInterval" class="carousel slide shadow-dark radius-border" data-ride="carousel">
                <div class="carousel-inner radius-border">

                  <?php

                  $sql_select_slide_image = "SELECT * FROM image_slide ORDER BY slide_id DESC";
                  $query_slide_image = $hyper->connect->query($sql_select_slide_image);
                  $slide_image = mysqli_fetch_array($query_slide_image);
                  $active = 1;
                  do {

                  ?>
                    <div class="carousel-item <?php if ($active == 1) {
                                                echo 'active';
                                              } ?>" data-interval="7000">
                      <img src="<?= $url ?>assets/img/slide/<?= $slide_image['image_name'] ?>" class="d-block w-100" alt="...">
                      <div class="carousel-caption d-none d-md-block pb-0">
                        <button type="button" onclick="DelImage(this)" value="<?= $slide_image['slide_id'] ?>" class="btn btn-sm hyper-btn-notoutline-danger w-100 mt-2"><i class="fal fa-image mr-1"></i>ลบรูปภาพ</button>
                      </div>
                    </div>
                  <?php $active = 0;
                  } while ($slide_image = mysqli_fetch_array($query_slide_image)); ?>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>

              </div>
              <!-- End Image Banner -->

              <form method="POST" id="addbannerimg" enctype="multipart/form-data">
                <div class="ml-auto mr-auto mb-3 text-center mt-3">
                  <img id="bannerimgnew" src="<?= $url ?>assets/img/slide/bannerani2.png" class="img-fluid" style="height: 100px;"></br>
                  <font class="text-muted">แนะนำขนาด 1920 x 700 Pixel</font></br>
                  <input type="hidden" value="1" name="bannerpass" />
                  <input type="file" style="display:none;" id="imgbannernew" name="imgbannernew" onchange="bannerURL(this,'new');" accept=".jpg,.png" />
                </div>
                <button type="submit" id="submitdatanew" class="d-none"></button>
              </form>

              <div class="row no-gutters ml-auto mr-auto pl-lg-4">
                <button onclick="uploadbanner('new') " class="btn col-12 mb-2 mb-md-0 col-md-5 mr-2 btn-sm hyper-btn-info w-100" type="button"><i class="fal fa-images mr-1"></i>เพิ่มรูปภาพ</button>
                <button onclick="submitdata('new')" class="btn col-12 col-md-6 btn-sm hyper-btn-notoutline-success w-100" type="submit"><i class="fal fa-check-circle mr-1"></i> อัพโหลดรูปภาพ</button>
              </div>

            </div>

          </div>

        </div>
      </div>

      <div class="card mt-4 shadow-dark radius-border hyper-bg-white ml-auto mr-auto">
        <div class="card-body">
          <h4 class="mt-0 mb-4 text-center">ตั้งค่าสปิน</h4>
          <form>
            <div class="form-group">
              <label for="pointuse" class="form-label">สปิน1ครั้ง/บาท</label>
              <input type="number" class="form-control" value="<?= $web['usepoint'] ?>" name="point_use">
            </div>
            <button class="btn hyper-btn-notoutline-success btn-sm btn-block">บันทึก</button>
          </form>
          <hr />
          <?php
          $i = 1;
          $getdataslice_wheel = $hyper->connect->query('SELECT * FROM setting_slices');
          while ($key = mysqli_fetch_assoc($getdataslice_wheel)) {
          ?>

            <h5>สไลด์ <?php echo $i; ?></h5>
            <div class="row">
              <div class="col-lg-6 my-1">
                <label for="images" class="form-label">รูปภาพ</label>
                <input type="text" class="form-control" value="<?= $key['image']; ?>" id="images<?php echo $i; ?>">
              </div>
              <div class="col-lg-6 my-1">
                <label for="message" class="form-label">ข้อมูล</label>
                <input type="text" class="form-control" value="<?= $key['message']; ?>" id="message<?php echo $i; ?>">
              </div>
              <div class="col-lg-4 my-1">
                <label for="typewheel" class="form-label">ประเภท ( item & point ) </label>
                <input type="text" class="form-control" value="<?= $key['reward_type']; ?>" id="typewheel<?php echo $i; ?>">
              </div>
              <div class="col-lg-4 my-1">
                <label for="point" class="form-label">จำนวนพอยที่ได้รับ </label>
                <input type="text" class="form-control" value="<?= $key['reward_point']; ?>" id="point<?php echo $i; ?>">
              </div>
              <div class="col-lg-4 my-1">
                <label for="percent" class="form-label">เปอร์เซ็นที่จะออก </label>
                <input type="text" class="form-control" value="<?= $key['percent']; ?>" id="percent<?php echo $i; ?>">
              </div>
            </div>
            <div class="">
              <button class="btn hyper-btn-notoutline-success mt-2 btn-sm" onclick="update(<?= $i ?>)">บันทึก</button>
            </div>
            <hr />
          <?php
            $i++;
          } ?>
        </div>
      </div>
      <!-- End Web Config -->

      <script src="<?= $url ?>assets/js/script.lp4r6hz.js"></script>

      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

      <script>
        function update(id) {
          var image = $("#images" + id).val();
          var infos = $("#message" + id).val();
          var reward_type = $("#typewheel" + id).val();
          var point = $("#point" + id).val();
          var percent = $("#percent" + id).val();
          $.post('plugin/update_wheel.php', {
            id,
            image,
            infos,
            reward_type,
            point,
            percent,
          }).then((res) => {
            Swal.fire({
              icon: 'success',
              title: 'สำเร็จ',
              text: 'บันทึกเรียบร้อย',
            }).then(() => {
              window.location.assign('/websetting')
            })
          }).catch((err) => {
            Swal.fire({
              icon: 'error',
              title: 'เกิดข้อผิดพลาด',
              text: err.responseJSON.msg,
            })
          })
        }
      </script>