<div class="card mt-4 shadow-dark radius-border hyper-bg-white ml-auto mr-auto">
    <div class="card-body">
        <h4 class="mt-0 mb-4 text-center">รหัสไก่ตัน</h4>
        <label></label>
        <textarea id="inputItemData" placeholder="ลงสินค้าที่ต้องการจะส่งให้กับลูกค้า..." class="form-control" rows="5"></textarea>
        <label class="mt-2 text-muted">รู้หรือไม่! ท่านสามารถเพิ่มสต๊อคหลายๆชิ้นได้โดยการพิมพ์ว่า &lt;batch&gt; ไว้ในบรรทัดแรก และบรรทัดที่เหลือให้ใส่ข้อมูลที่จะส่งให้ลูกค้า <a href="#" onclick="$('#inputItemData').val('<batch>\n' + $('#inputItemData').val()); $('#inputItemData').focus();">ตัวอย่าง</a></label>
        <button class="btn hyper-btn-notoutline-success btn-block" onclick="addStock()">เพิ่มสต๊อก</button>
        <hr>
        <div class="table-responsive mt-3">
            <table id="datatable" class="table table-hover text-center w-100">
                <thead class="hyper-bg-dark">
                    <tr>
                        <th scope="col" style="width:120px;">id</th>
                        <th scope="col">รายละเอียด</th>
                        <th scope="col">ผู้รับ</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $sql_select_pay = "SELECT * FROM gaitun_stock";
                    $query_pay = $hyper->connect->query($sql_select_pay);
                    $total_pay_row = mysqli_num_rows($query_pay);

                    if ($total_pay_row > 0) {
                        $pay = mysqli_fetch_array($query_pay);
                        do {
                    ?>
                            <tr>
                                <td><?= $pay['id']; ?></th>
                                <td><?= $pay['content']; ?></th>
                                <td><?= $pay['owner']; ?></th>
                            </tr>
                    <?php } while ($pay = mysqli_fetch_array($query_pay));
                    } ?>


                </tbody>
            </table>
        </div>
    </div>
    <hr>
</div>
</div>
<div class="card mt-4 shadow-dark radius-border hyper-bg-white ml-auto mr-auto">
    <div class="card-body">
        <h4 class="mt-0 mb-4 text-center">รหัสไก่ตัน</h4>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function addStock() {
        let data = $("#inputItemData").val();
        $.post('plugin/add_gaitun.php', {
            data
        }).then((res) => {
            Swal.fire({
                icon: 'success',
                title: 'สำเร็จ',
                text: 'บันทึกเรียบร้อย',
            }).then(() => {
                window.location.assign('/gaitun')
            })
        }).catch((err) => {
            Swal.fire({
                icon: 'error',
                title: 'เกิดข้อผิดพลาด',
                text: err.responseJSON.msg,
            })
        })
    }
</script>