      <!-- Game Type -->

      <h3 class="text-center mt-4 mb-4">--- เกมทั้งหมดในระบบ ---</h3>
      <center><button class="btn hyper-btn-info my-2 my-sm-0 w-100" type="button" data-toggle="modal" data-target="#addgamemodal"><i class="fal fa-plus-square mr-1"></i> เพิ่มเกมใหม่เข้าระบบ</button></center>

      <!-- Add Game Modal -->
      <div class="modal fade" id="addgamemodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content border-0 radius-border-2 hyper-bg-white">
            <div class="modal-header hyper-bg-dark">
              <h6 class="modal-title"><i class="fal fa-plus-square mr-1"></i> เพิ่มเกมใหม่เข้าระบบ</h6>
            </div>
            <div class="modal-body text-center">

              <!-- Card Example -->
              <div class="media m-auto">
                <img id="gamelogoimgnew" src="<?= $url ?>assets/img/logoani_236x236.jpg" class="align-self-center mr-3 rounded-circle d-none d-md-block" width="70px;">
                <div class="media-body text-center text-md-left">
                  <img id="gamelogoresimgnew" src="<?= $url ?>assets/img/logoani_236x236.jpg" class="ml-auto mr-auto rounded-circle d-block d-md-none" width="70px;">
                  <h4 class="mt-0 mb-1" id="gamenamenew">GAMENAME</h4>
                  <font class="text-muted">แนะนำขนาด 120 x 120 Pixel</font>
                </div>
              </div>
              <!-- End Card Example -->

              <form id="addnewgame" method="POST" enctype="multipart/form-data">

                <div class="input-group mb-2 mt-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text hyper-bg-dark border-dark"><i class="fal fa-gamepad-alt"></i></span>
                  </div>
                  <input id="nametxtgamenew" name="nametxtgamenew" onkeyup="txtgamepreview(this,'new')" maxlength="32" type="text" class="form-control form-control-sm hyper-form-control" placeholder="ชื่อเกมที่ต้องการเพิ่ม" required autocomplete="off">
                </div>
                <input type="file" style="display:none;" id="imggamelogonew" name="imggamelogonew" onchange="gamelogoURL(this,'new');" accept=".jpg,.png"/>
                <button id="uploadimggamelogo" onclick="uploadgamelogo('new')" type="button" class="btn btn-sm hyper-btn-info w-100"><i class="fal fa-image mr-1"></i>เพิ่มรูปภาพ</button>
                <button type="submit" id="submitdatanew" class="d-none"></button>
              </form>

            </div>
            <div class="modal-footer p-2 border-0">
              <button type="button" onclick="submitdata('new')" class="btn hyper-btn-notoutline-success"><i class="fal fa-plus-square mr-1"></i>เพิ่มเกม</button>
              <button type="button" class="btn hyper-btn-notoutline-danger" data-dismiss="modal"><i class="fad fa-times-circle mr-1"></i>ยกเลิก</button>
            </div>
          </div>
        </div>
      </div>
      <!-- End Add Game Modal -->

      <!-- Card -->
      <div class="row no-gutters mt-3">

      <?php

        $sql_select_game = "SELECT * FROM game_type ORDER BY game_id DESC";
        $query_game = $hyper->connect->query($sql_select_game);
        $total_game_row = mysqli_num_rows($query_game);
        $game = mysqli_fetch_array($query_game);

        if($total_game_row <= 0){
      ?>
        <h4 class="text-center w-100 mt-4">ไม่มีข้อมูลในขณะนี้</h4>
      <?php 
        }else{
          do{

            $gid = $game['game_id'];
            $data_ready_selled = "SELECT count(data_id) AS 'totaldata' FROM game_data WHERE game_id = $gid AND selled = 0";
            $ready_selled_row = $hyper->connect->query($data_ready_selled)->fetch_array();
      ?>
        <div class="col-6 col-lg-4 p-2">
            <div class="card shadow-dark radius-border-6 hyper-bg-white text-center p-2 h-100 hyper-card">
                <div class="media mb-2">
                    <img src="<?= $url ?>assets/img/game/<?= $game['game_image']; ?>" class="align-self-center mr-3 rounded-circle d-none d-md-block" width="70px;">
                    <div class="media-body text-center text-md-left">
                      <img src="<?= $url ?>assets/img/game/<?= $game['game_image']; ?>" class="ml-auto mr-auto rounded-circle d-block d-md-none" width="70px;">
                      <h4 class="mt-0 mb-1"><?= $game['game_name']; ?></h4>
                      <font class="text-muted"><?= number_format($ready_selled_row['totaldata'],0); ?> ไอดี</font>
                    </div>
                </div>
                  <button class="btn btn-sm hyper-btn-info mt-3" type="button" data-toggle="modal" data-target="#editgamemodal<?= $game['game_id']; ?>"><i class="fal fa-edit mr-1"></i> แก้ไข</button>
                  <button class="btn btn-sm hyper-btn-notoutline-danger mt-2" onclick="DelGame(this)" value="<?= $game['game_id']; ?>" type="button"><i class="fal fa-trash-alt mr-1"></i> ลบ</button>
            </div>
        </div>
        
        <!-- Card Game Edit Modal -->
        <div class="modal fade" id="editgamemodal<?= $game['game_id']; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content border-0 radius-border-2 hyper-bg-white">
              <div class="modal-header hyper-bg-dark">
                <h6 class="modal-title"><i class="fal fa-plus-square mr-1"></i> อัพเดท</h6>
              </div>
              <div class="modal-body text-center">
  
                <!-- Card Example -->
                <div class="media m-auto">
                  <img id="gamelogoimg<?= $game['game_id']; ?>" src="<?= $url ?>assets/img/game/<?= $game['game_image']; ?>" class="align-self-center mr-3 rounded-circle d-none d-md-block" width="70px;">
                  <div class="media-body text-center text-md-left">
                    <img id="gamelogoresimg<?= $game['game_id']; ?>" src="<?= $url ?>assets/img/game/<?= $game['game_image']; ?>" class="ml-auto mr-auto rounded-circle d-block d-md-none" width="70px;">
                    <h4 class="mt-0 mb-1" id="gamename<?= $game['game_id']; ?>"><?= $game['game_name']; ?></h4>
                    <font class="text-muted">แนะนำขนาด 120 x 120 Pixel</font>
                  </div>
                </div>
                <!-- End Card Example -->
  
                <form id="updategame<?= $game['game_id']; ?>" method="POST" enctype="multipart/form-data">
  
                  <div class="input-group mb-2 mt-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text hyper-bg-dark border-dark"><i class="fal fa-gamepad-alt"></i></span>
                    </div>
                    <input id="nametxtgame<?= $game['game_id']; ?>" onkeyup="txtgamepreview(this,'<?= $game['game_id']; ?>')" maxlength="32" type="text" value="<?= $game['game_name']; ?>" class="form-control form-control-sm hyper-form-control" placeholder="ชื่อเกม" required autocomplete="off">
                  </div>
                  <input type="file" style="display:none;" id="imggamelogo<?= $game['game_id']; ?>" name="imggamelogo<?= $game['game_id']; ?>" onchange="gamelogoURL(this,'<?= $game['game_id']; ?>');" accept=".jpg,.png" />
                  <button id="uploadimggamelogo" onclick="uploadgamelogo('<?= $game['game_id']; ?>')" type="button" class="btn btn-sm hyper-btn-info w-100"><i class="fal fa-image mr-1"></i>เปลี่ยนรูปภาพ</button>
                  <button type="submit" id="updatedata<?= $game['game_id']; ?>" class="d-none"></button>
                </form>
  
              </div>
              <div class="modal-footer p-2 border-0">
                <button type="button" onclick="updatedata('<?= $game['game_id']; ?>')" class="btn hyper-btn-notoutline-success"><i class="fal fa-plus-square mr-1"></i>อัพเดท</button>
                <button type="button" class="btn hyper-btn-notoutline-danger" data-dismiss="modal"><i class="fad fa-times-circle mr-1"></i>ยกเลิก</button>
              </div>
            </div>
          </div>
        </div>
        <!-- End Card Game Edit Modal -->

        <?php }while ($game = mysqli_fetch_array($query_game));} ?>

      </div>
      <!-- End Card -->

      <script src="<?= $url ?>assets/js/script.mn1w9ok.js"></script>
      <!-- End Game Type -->