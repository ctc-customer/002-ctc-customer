<?php
    $getdataslice_wheel = 'SELECT * FROM setting_slices';
    $results = $hyper->connect->query($getdataslice_wheel);

?>

<div class="card shadow-dark radius-border">
    <div class="card-body p-0 text-center pt-4">
        <h4>สุ่มของรางวัล</h4>
        <hr/>
        <div id="superwheel"></div>
        <button class="spin-button btn btn-sm hyper-btn-success my-3">สุ่มเลยย ( <?php echo $usepoint; ?> บาท )</button>
        <p>ยอดเงินคงเหลือ <span id="pointnow"><?php echo $points; ?></span> บาท</p>
    </div>
</div>
<div class="card mt-4 shadow-dark radius-border hyper-bg-white ml-auto mr-auto">
    <div class="card-body">
        <h4 class="mt-0 mb-4 text-center">ประวัติการสุ่ม</h4>
        <div class="table-responsive mt-3">
            <table id="datatable" class="table table-hover text-center w-100">
                <thead class="hyper-bg-dark">
                    <tr>
                        <th scope="col" style="width:120px;">id</th>
                        <th scope="col">ของที่ได้รับ</th>
                        <th scope="col">รายละเอียด</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $sql_select_pay = "SELECT * FROM history_reward WHERE ac_id = $ac_id";
                    $query_pay = $hyper->connect->query($sql_select_pay);
                    $total_pay_row = mysqli_num_rows($query_pay);

                    if ($total_pay_row > 0) {
                        $pay = mysqli_fetch_array($query_pay);
                        do {
                    ?>
                            <tr>
                                <td><?= $pay['id']; ?></th>
                                <td><?= $pay['reward_info']; ?></th>
                                <td><?= $pay['detail']; ?></th>
                            </tr>
                    <?php } while ($pay = mysqli_fetch_array($query_pay));
                    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function() {
        $('#superwheel').superWheel({
            slices: [<?php while ($row = mysqli_fetch_assoc($results)) {?> {
                    text: "<?php echo $row['image']; ?>",
                    value: <?php echo $row['id']; ?>,
                    message: "<?php echo $row['message']; ?>",
                    background: "#424242",
                    color: "#fff"
                }, <?php } ?>],
            width: 700,
            frame: 1,
            type: "spin",
            text: {
                type: "image",
                color: "#ccc",
                size: 20,
                offset: 8,
                orientation: "h",
                arc: false
            },
            line: {
                color: "#939393"
            },
            outer: {
                color: "#939393"
            },
            inner: {
                color: "#939393"
            },
            center: {
                rotate: true,
            },
            marker: {
                animate: "true"
            }
        });
        var tick = new Audio('https://www.22codes.com/demo/javascript/superwheel/dist/tick.mp3');
        $(document).on('click', '.spin-button', function(e) {
            e.preventDefault();
            $('.spin-button').attr('disabled', 'disabled');
            $.post('plugin/start_spin.php', {start_spin: true}).done((res) => {
                $('#superwheel').superWheel('start', 'value', res.spin_value);
                setTimeout(function() {
                    $('#pointnow').html(res.pointnow);
                }, 5000);
            }).fail((err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'สุ่มรหัสปักธง',
                    text: err.responseJSON.msg,
                }).then(() => {
                    window.location.assign('/spin')
                })
            })

        });
        $('#superwheel').superWheel('onComplete', function(results) {
            Swal.fire({
                icon: 'success',
                title: 'สุ่มรหัสปักธง',
                text: "คุณได้รับ "+ results.message,
                footer: 'หากได้รับของกรุณาไปดูประวัติการสุ่ม</a>'
            }).then(() => window.location.assign('/spin'))
            $('.spin-button').removeAttr("disabled");
        });
        $('#superwheel').superWheel('onStep', function(results) {

            if (typeof tick.currentTime !== 'undefined')
                tick.currentTime = 0;

            tick.play();

        });
    })
</script>