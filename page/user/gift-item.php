<div class="container my-3 aos-init aos-animate" data-aos="fade-up">
  <div class="page-main-title">
    <h1 class="title">บริการ Gift Item ต่างๆ</h1>
    <div class="subtitle">ในเกม Roblox</div>
  </div>
  <div class="mb-0 mt-2 mt-md-4">
    <img src="https://lawabux.com/assets/images/category/15-cover.png?update=1615960209" class="img-fluid rounded-xl" style="border-bottom-left-radius: 0px!important;border-bottom-right-radius:0px!important;">
  </div>
  <div class="box-panel" style="border-top-left-radius: 0px;border-top-right-radius:0px;">
    <div class="py-2 font12">
    </div>
    <div class="row">
      <div class="col-12 col-lg-6 mb-4 aos-init aos-animate" data-aos="fade-up" data-aos-delay="50">
        <a href="bloxFruit" class="category-panel">
          <div class="category-image">
            <img src="https://lawabux.com/assets/images/category/5-cover.png?update=1615956975" class="img-fluid">
          </div>
          <div class="category-details bg-dark">
            <div class="category-name text-white">ผลปีศาจ-BloxFruit</div>
            <div class="category-description text-white">บริการเติมผลปีศาจถาวร</div>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
