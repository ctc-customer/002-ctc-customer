<div class="container my-2 my-md-5 itemshop aos-init aos-animate" data-aos="fade-up">
  <form class="itemshop-form">
    <input type="hidden" name="action" value="/action/itemshop/buy">
    <input type="hidden" name="itemId" value="36">
    <div class="row mb-5">
      <div class="col-12 col-md-4">
        <div class="content-rounded bg-gradient-2">
          <div class="content-body bg-gradient-black-1 p-0">
            <img src="https://cdn.discordapp.com/attachments/897814868130938890/960485570935681028/36-product.png" class="img-fluid rounded-xl-2" style="border-bottom-left-radius: 0px!important;border-bottom-right-radius:0px!important;">
          </div>
        </div>
      </div>
      <div class="col-12 col-md-8">
        <div class="content-rounded-sm bg-gradient-1">
          <div class="content-body bg-gradient-black-1 px-3 py-4">
            <h1 class="page-text-title mb-0">ผล Bomb-BloxFruit</h1>
            <div class="font-light">กดผลจากแมพ BloxFruit</div>
          </div>
        </div>
        <div class="content-rounded bg-dark mt-4">
          <div class="text-center text-white text-shadow py-3">
            <h3 class="mb-0">
              <small>ราคา</small>
              <b>5</b>บาท
              <small class="font-weight-light">/ต่อชิ้น</small>
            </h3>
          </div>
        </div>
      </div>
    </div>
    <div class="w-100 mt-3">
      <div class="">
        <div class="row py-3 py-md-5">
          <div class="content-rounded bg-gradient-4 col-12 col-md-10 col-xl-8 m-auto">
            <div class="content-body bg-gradient-black-1 py-4 px-3">
              <div class="form-group">
                <div class="font-bold">
                  <i class="fad fa-edit"></i>สินค้านี้จำเป็นต้องกรอกข้อมูลเพิ่มเติม</div>
              </div>
              <div class="form-group row">
                <label for="input-0" class="col-12 col-md-3 col-form-label text-md-right text-sm-left">กรอก ID :</label>
                <div class="col-12 col-md-9">
                  <input id="input-0" type="text" class="form-control" placeholder="Username" name="input[0]" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-1" class="col-12 col-md-3 col-form-label text-md-right text-sm-left">กรอก Pass :</label>
                <div class="col-12 col-md-9">
                  <input id="input-1" type="text" class="form-control" placeholder="Password" name="input[1]" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-2" class="col-12 col-md-3 col-form-label text-md-right text-sm-left">ทำเมลล์แดงก่อนเติม :</label>
                <div class="col-12 col-md-9">
                  <input id="input-2" type="text" class="form-control" placeholder="ทำแล้ว / ทำไม่เป็นกรุณาทักเพจก่อนซื้อสินค้านี้" name="input[2]" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-3" class="col-12 col-md-3 col-form-label text-md-right text-sm-left">กลับโลก 1 ก่อนสั่งซื้อ :</label>
                <div class="col-12 col-md-9">
                  <input id="input-3" type="text" class="form-control" placeholder="กลับแล้ว / ยังไม่กลับ / กลับไม่เป็น" name="input[3]" required="">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-4" class="col-12 col-md-3 col-form-label text-md-right text-sm-left">ไม่เข้าแทรกระหว่างสั่งซื้อ :</label>
                <div class="col-12 col-md-9">
                  <input id="input-4" type="text" class="form-control" placeholder="เข้าใจ / ไม่เข้าใจ" name="input[4]" required="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-9 mx-auto my-3 my-md-4 content-rounded bg-gradient-5">
      <div class="content-body bg-gradient-black-1">
        <div class="py-4 my-4">
          <div class="py-2">
            <div class="col-12 col-xl-8 m-auto">
              <input type="tel" class="form-control form-control-lg text-center" name="each" placeholder="จำนวนสินค้า (ต่อชิ้น)" autofocus="" maxlength="10">
              <div class="bg-dark text-white text-center mb-3 p-2 font-light rounded-lg mt-3">ราคา
                <span class="summaryEach">
                </span>5.00 บาท ค่าใช้จ่าย
                <b class="text-warning px-1 summaryCredit font-bold">0 บาท</b>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-lg btn-block btn-outline-success">
                  <i class="fal fa-cart-plus"></i>สั่งซื้อสินค้า</button>
              </div>
            </div>
          </div>
          <div class="py-2 text-center">
            <div class="d-inline-block py-2 px-3 text-dark">คุณมียอดคงเหลือ
              <b class="text-dark">0฿</b>
              <a class="font-weight-light text-dark font12" href="topup">(
                <i class="fas fa-sm fa-plus-circle text-dark"></i>เติมเงิน)</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
