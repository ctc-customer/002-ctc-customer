<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<?php 
class HitoriAPI{
    public $licensekey = 'NDBFdFy2VsMYGT4CS3Zv7nilQzDNlpod1WMEyHvESjag6CxQ01KuU5hrbJo0cWqK'; //กรุณาใส่คีย์ คีย์จะแสดงอยู่ที่ https://n.hitorikungz.tk

    public $api = 'https://api.hitori.tk/v2.php';
    //public $api = 'http://api.p5shop.in.th/gift.php';
    function truewallet($gift){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->api.'?key='.$this->licensekey.'&gift='.$gift,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array('Content-Type: application/json',),
        ));
        $this->response = curl_exec($curl);
        curl_close($curl);
        $this->data = json_decode($this->response, true);
        return $this->data;
    }

}

if (isset($_POST['payment-truewallet'])) {
    $api = new HitoriAPI();
    $gift = $_POST['transaction'];
    $result = $api->truewallet($gift);
    
    if($result['code'] == '200'){  //เมื่อรายการสำเร็จ

    $amount =  $result['amount'];
    
    $update_data_sql = "UPDATE accounts SET points = points+ '$amount' WHERE username = '$username'";
    $query_data_update = $hyper->connect->query($update_data_sql);
    if(!$query_data_update){
        echo "
        <script>
        Swal.fire({
            icon: 'warning',
            title: 'ไม่สามารถทำรายการได้',
            text: 'เกิดข้อผิดผลาดกับระบบ โปรดลองใหม่อีกครั้ง!'
        }).then(() => {
        })    
        </script>  
        ";
    }else{
        echo "
        <script>
        Swal.fire({
            icon: 'success',
            title: 'สำเร็จ',
            text: 'คุณได้เติมเงินเข้าระบบเป็นจำนวน $amount บาท'
        }).then(() => {
            window.location = '';
        })    
        </script>
        ";   
    }    

    }else{  //เมื่อรายการไม่สำเร็จ
        $msg = $result['msg'];
        echo "
        <script>
        Swal.fire({
            icon: 'warning',
            title: 'ไม่สามารถทำรายการได้',
            text: '$msg!'
        }).then(() => {
        })    
        </script>  
        ";

    }
}




?>
<!-- Pay Form -->
      <div class="card shadow-dark radius-border">
        <div class="card-body p-0 text-center pt-4">

            <img src="<?= $url ?>assets/img/tw.png" style="width: 15%;" class="mb-4 pr-4 border-right">
            <img src="<?= $url ?>assets/img/wallet-logo.png" style="width: 40%;" class="mb-4 pl-4">
            </p>

            <h4>ใส่ลิ้งซองของขวัญ</h4>
            <h6>คงเหลือ <?= $points; ?> Points</h6>
            <form method="POST" action="">
            <input type="text" name="transaction" class="text-center form-control form-control-sm hyper-form-control ml-auto mr-auto" placeholder="กรอกลิ้งซองอั่งเปา" style="max-width:350px;width:80%;border: 1px solid #343a40;" autocomplete="off">
            <small id="giftlinkHelp" class="form-text" style="opacity: 0.7;">ตัวอย่างลิ้ง : https://gift.truemoney.com/campaign/?v=cofi9...</small>
            <button name="payment-truewallet" type="submit" class="btn btn-sm hyper-btn-success mt-3 ml-auto mr-auto w-100 mb-3" style="max-width:350px;"><i class="far fa-check-circle pr-1 pt-1"></i> ตรวจสอบการทำรายการ</button></br>
			</form>
            <b>วิธีการเติมเงิน</b></br>
			1. เข้าไปที่แอป  Truewallet </br>
			2. ไปที่  บัตรเติมเงิน & ของขวัญ  ->  ส่งซองของขวัญ  <a href="https://www.img.in.th/images/78bc8881c2d195fdc3aa259ff1f7d278.jpg" target="_blank">[ดูรูปตัวอย่าง]</a></br>
			3. หลังจากเข้าที่  ส่งซองของขวัญ  แล้วให้กรอก  จำนวนเงิน  และ  จำนวนผู้รับซอง  แนะนำให้กรอกจำนวนแค่ 1 ไม่งั้นจะได้เงินไม่เต็มจำนวน <a href="https://www.img.in.th/images/7c4782d43a430c3f0de7388a5dba722a.jpg" target="_blank">[ดูรูปตัวอย่าง]</a></br>
			4. หลังจากกรอกข้อมูลแล้วให้กด  สร้างซองของขวัญ  และจะขึ้นหน้ายืนยันข้อมูลให้กด  ยืนยัน  <a href="https://www.img.in.th/images/1a264cbb4b8d7c0827466c33559dae32.jpg" target="_blank">[ดูรูปตัวอย่าง]</a></br>
			5. หลังจากกดยืนยันแล้วจะมี  Link  มาให้กด  คัดลอก  และนำไปเติมตามเว็บได้เลย <a href="https://www.img.in.th/images/89312ce1d42342befb9b7c1877c175ac.jpg" target="_blank">[ดูรูปตัวอย่าง]</a></br>
            <small style="opacity: 0.7;">Truewallet Gift API By <a href="https://www.facebook.com/pagehyperstudio" class="text-sky" target="_blank" >Hyper Studio</a>.</small>
            <div class="mt-4"></div>
      </div>
      </div>
      <!-- End Pay Form -->

      <script src="<?= $url ?>assets/js/script.ty7j9lx.js"></script>