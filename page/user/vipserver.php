<div class="container my-3">
  <div class="d-block w-100 text-center my-5 aos-init aos-animate" data-aos="fade-up">
    <h1 class="page-text-title m-0">Buy Robux R$</h1>
    <h6>ซื้อ R$ ด้วย Configure game</h6>
  </div>
  <div class="row aos-init aos-animate" data-aos="fade-up">
    <div class="col-12 col-md-7 col-lg-5 mx-auto">
      <div class="content-rounded bg-gradient-1 text-center">
        <div class="content-body bg-gradient-black-1 py-4 shadow p-3 mb-5 bg-white rounded">
          <div class="font-light font-small">Rate 8/THB</div>
          <h2 class="mb-0 text-warning">0 R$</h2>
          <div>พร้อมจำหน่าย</div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-4 mt-md-5 aos-init aos-animate" data-aos="fade-up">
    <div class="col-12 col-md-5">
      <div class="content-rounded bg-gradient-2 shadow p-3 mb-5 bg-white rounded">
        <div class="content-body bg-gradient-black-1 p-3">
          <div id="step-robloxusername">
            <form id="form-roblox_username">
              <h2 class="mb-0">Roblox Username</h2>
              <div class="text-muted font-weight-light">กรุณากรอกชื่อผู้ใช้งาน Roblox ของคุณ</div>
              <div class="mt-3">
                <input type="text" class="form-control form-control-lg" name="roblox_username" id="roblox_username" placeholder="Roblox Username" required="">
              </div>
              <div class="mt-1">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text text-success">R$</span>
                  </div>
                  <input type="number" class="form-control form-control-lg" name="robux" id="robux" placeholder="จำนวนที่ต้องการซื้อ Robux (R$)" value="100">
                  <div class="input-group-append">
                    <span class="input-group-text">Rate 8/THB</span>
                  </div>
                </div>
              </div>
              <div class="my-2">
                <div class="row">
                  <div class="col-4 mb-1">
                    <button type="button" class="btn btn-block btn-outline-success btn-select-robux" data-robux="100">100 R$</button>
                  </div>
                  <div class="col-4 mb-1">
                    <button type="button" class="btn btn-block btn-outline-success btn-select-robux" data-robux="250">250 R$</button>
                  </div>
                  <div class="col-4 mb-1">
                    <button type="button" class="btn btn-block btn-outline-success btn-select-robux" data-robux="500">500 R$</button>
                  </div>
                  <div class="col-4 mb-1">
                    <button type="button" class="btn btn-block btn-outline-success btn-select-robux" data-robux="750">750 R$</button>
                  </div>
                  <div class="col-4 mb-1">
                    <button type="button" class="btn btn-block btn-outline-success btn-select-robux" data-robux="1000">1,000 R$</button>
                  </div>
                  <div class="col-4 mb-1">
                    <button type="button" class="btn btn-block btn-outline-success btn-select-robux" data-robux="5000">5,000 R$</button>
                  </div>
                </div>
              </div>
              <div class="mt-3">
                <div class="bg-dark py-3 text-center content-body ">ซื้อ
                  <b class="text-success">100 R$</b>
                  <b class="text-white">เป็นจำนวนเงินทั้งสิ้น</b>
                  <b class="total-amount text-white">13</b>
                  <b class="text-white">THB</b>
                </div>
              </div>
              <div class="mt-3">
                <button id="btn-roblox-username" type="submit" class="btn btn-lg btn-block btn-success">
                  <i class="fal fa-check-circle"></i>ตรวจสอบข้อมูลชื่อผู้ใช้งาน</button>
              </div>
            </form>
            <form id="form-game_link" style="display:none;">
              <h2 class="mb-0">Configure game</h2>
              <div class="text-muted font-weight-light">ตั้งค่าเกมเพื่อรับ R$</div>
              <div class="mt-3">
                <img src="" style="height:30px" class="rounded-circle roblox-avatar">
                <span class="roblox-username">
                </span>
              </div>
              <div class="mt-2">
                <a href="#" target="_blank" class="game-link">
                  <img src="" class="img-fluid img-thumbnail" id="game-image">
                </a>
              </div>
              <div class="mt-3">
                <a class="btn btn-danger btn-block game-config" target="_blank">
                  <i class="fas fa-cogs"></i>Configure Game</a>
                <div class="py-2 text-center">กรุณากำหนดเป็น Paid และกำหนด Price เท่ากับ
                  <div class="pt-1">
                    <b class="bg-success total-set-price text-white px-2 py-1 rounded-lg">143 Robux</b>
                    <small>ขณะนี้คุณตั้งเป็น
                      <span class="total-current-price">
                      </span>
                    </small>
                  </div>
                </div>
              </div>
              <div class="mt-3">
                <div class="bg-dark py-3 text-center content-body">คุณจะได้รับ
                  <b class="text-success total-robux">100 R$</b>โดยชำระเงิน
                  <b class="total-amount">13</b>THB</div>
              </div>
              <div class="mt-4">
                <input type="hidden" name="roblox_username" value="">
                <input type="hidden" name="roblox_userId" value="">
                <input type="hidden" name="placeId" value="">
                <input type="hidden" name="universeId" value="">
                <input type="hidden" name="robux" value="">
                <button id="btn-place-check" type="button" class="btn btn-lg btn-block btn-warning">
                  <i class="fal fa-sync"></i>ตรวจสอบข้อมูล Game ใหม่อีกครั้ง</button>
                <div class="text-danger text-center">คุณจะได้รับ Robux เข้าบัญชีภายใน 5 - 7 วันหลังจากสั่งซื้อ</div>
                <button id="btn-vip-buy" type="submit" class="btn btn-lg btn-block btn-success">
                  <i class="fal fa-check-circle"></i>ยืนยันการสั่งซื้อ Robux</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-7">
      <div class="content-rounded bg-gradient-3">
        <div class="content-body bg-gradient-black-1 p-3">
          <div class="helper-lists">
            <h2 class="mb-0">ขั้นตอนการซื้อ R$</h2>
            <div class="text-muted font-weight-light">วิธีและการดำเนินการรูปแบบ Configure game</div>
            <ol class="mt-2 text-secondary font-weight-light">
              <li data-step="1" class="font-weight-bold">กรุณากรอก Roblox Username เพื่อให้ระบบตรวจสอบข้อมูลเกมส์ของท่าน</li>
              <li data-step="2" class="font-weight-bold">กรอกจำนวน Robux ที่ต้องการซื้อโดยมีค่าบริการ
                <b>Rate 8/THB</b>
              </li>
              <li data-step="3">กรุณากดเข้า
                <a href="#" class="font-weight-bold game-config" target="_blank">
                  <i class="fas fa-link"></i>Game Configure</a>เพื่อเข้าไปตั้งค่าที่
                <b>Configure this Place</b>
              </li>
              <li data-step="4">กรุณาตั้งค่า Private Server เป็น Public Server</li>
              <li data-step="5">กรุณาไปที่แถบ "Access" เลื่อนหา Private servers เปลี่ยนเป็น "Paid" และกำหนดราคาเป็น
                <b class="total-set-price text-success">143 Robux</b>เพื่อที่จะได้รับ R$ เข้าในระบบบัญชี
                <b class="total-robux">100 R$</b>
              </li>
              <li data-step="6">ยืนยันการสั่งซื้อ Robux R$</li>
              <li data-step="7" class="text-warning">โดย Robux จะเข้าบัญชีภายใน 5-7 วัน</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
