<div class="container my-3 aos-init aos-animate" data-aos="fade-up">
  <div class="page-main-title">
    <h1 class="title">ผลปีศาจ-BloxFruit</h1>
    <div class="subtitle">บริการเติมผลปีศาจถาวร</div>
  </div>
  <div class="mb-0 mt-2 mt-md-4">
    <img src="https://lawabux.com/assets/images/category/5-cover.png?update=1615956975" class="img-fluid rounded-xl" style="border-bottom-left-radius: 0px!important;border-bottom-right-radius:0px!important;">
  </div>
  <div class="box-panel" style="border-top-left-radius: 0px;border-top-right-radius:0px;">
    <div class="py-2 font12">
    </div>
    <div class="item-list">
      <div class="row py-2">
        <input type="hidden" id="categoryId" value="5">
        <input type="hidden" id="order" value="default">
        <input type="hidden" id="row" value="0">
        <div class="col-12 col-md-6">
          <div class="input-group">
            <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-align-justify"></i>สินค้าตามระบบ</button>
            <div class="dropdown-menu dropdown-menu-left">
              <button class="dropdown-item list-order" data-order="default">
                <i class="fas fa-align-justify"></i>สินค้าตามระบบ</button>
              <button class="dropdown-item list-order" data-order="lasted">
                <i class="fas fa-angle-double-up"></i>สินค้าใหม่ล่าสุด</button>
              <button class="dropdown-item list-order" data-order="older">
                <i class="fas fa-angle-double-down"></i>สินค้าเก่าสุด</button>
              <button class="dropdown-item list-order" data-order="recommend">
                <i class="fas fa-highlighter"></i>สินค้าแนะนำ</button>
              <button class="dropdown-item list-order" data-order="priceHighest">
                <i class="fas fa-arrow-up"></i>ราคามากที่สุด</button>
              <button class="dropdown-item list-order" data-order="priceLowest">
                <i class="fas fa-arrow-down"></i>ราคาน้อยที่สุด</button>
            </div>
          </div>
        </div>
        <div class="col-12 offset-md-2 col-md-4 mt-3 mt-md-0 text-left text-md-right">
          <form method="get" action="https://lawabux.com/category/DevilFruit-BloxFruit">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="ค้นหาสินค้า" name="s">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submbit">
                  <i class="fas fa-search"></i>ค้นหา</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div id="list-item" class="row">
        <div class="col-6 col-md-4 col-lg-3">
          <a class="item product-panel" href="Bomb-bloxFruit">
            <div class="product-image">
              <img src="https://cdn.discordapp.com/attachments/897814868130938890/960485570935681028/36-product.png" class="img-fluid rounded">
            </div>
            <div class="product-name text-white">ผล Bomb-BloxFruit</div>
            <div class="product-description text-white">กดผลจากแมพ BloxFruit</div>
            <hr class="text-white">
            <div class="product-price text-white">ราคา
              <b class="text-white">5฿</b>
            </div>
          </a>
        </div>
      </div>
      <div id="pagination">
        <div class="pagging mt-4 d-flex justify-content-center">
          <nav>
            <ul class="pagination">
              <li class="page-item active">
                <span class="page-link">1
                  <span class="sr-only">(current)</span>
                </span>
              </li>
              <li class="page-item">
                <span class="page-link">
                  <a href="/shop/2" data-ci-pagination-page="2">2</a>
                </span>
              </li>
              <li class="page-item">
                <span class="page-link">
                  <a href="/shop/3" data-ci-pagination-page="3">3</a>
                </span>
              </li>
              <li class="page-item">
                <span class="page-link">
                  <a href="/shop/2" data-ci-pagination-page="2" rel="next">></a>
                  <span aria-hidden="true">
                  </span>
                </span>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <div class="py-3">
      <div class="text-right text-md-center lead font-small">รายการสินค้าทั้งหมด 33 รายการ</div>
    </div>
  </div>
</div>
