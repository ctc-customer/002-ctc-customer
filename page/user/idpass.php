<div class="text-center">
  <h1 class="title">Robux ID+PASS </h1>
  <div class="subtitle">Username + Password</div>
</div>
<div class="container my-3">
  <form class="postForm row my-4">
    <input type="hidden" name="action" value="/action/shop/buyIdpass">
    <div class="col-12 col-md-6 m-auto aos-init aos-animate " data-aos="fade-up">
      <div class="box-panel mt-3 text-center">
        <div class="py-4">
          <h3>กรอกชื่อผู้ใช้และรหัสผ่าน</h3>
          <h6 class="text-info">Rate 3-8</h6>
          <div class="row justify-content-center">
            <div class="col-lg-10 col-12 py-3">
              <div class="form-group">
                <input type="text" class="form-control form-control-lg" placeholder="ชื่อผู้ใช้งานในเกม Roblox" id="roblox_username" name="roblox_username" autofocus="" required="">
              </div>
              <div class="form-group">
                <input type="text" class="form-control form-control-lg" placeholder="รหัสผ่านในเกม Roblox" id="roblox_password" name="roblox_password" required="">
              </div>
            </div>
            <div class="col-lg-10 col-12 font-kanit">
              <a class="text-danger" href="https://lawabux.com/login?return_url=/robux/idpass">กรุณาเข้าสู่ระบบเพื่อสั่งซื้อต่อไป</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 row justify-content-center my-3 my-md-5">
      <div class="col-12">
        <div class="row">
          <div class="col-md-3 col-6 my-3 aos-init aos-animatex" data-aos="fade-up" data-aos-delay="50">
            <div class="content-rounded bg-gradient-1 card-hover shadow p-3 mb-5 bg-white rounded">
              <div class="text-center">
                <label for="robux-139" class="w-100 mb-0 py-3 cursor-notallowed content-body bg-gradient-black-1">
                  <input hidden="" type="radio" id="robux-139" name="amount" value="139" data-robux="400" data-amount="139">
                  <img src="https://lawabux.com/assets/images/icon-idpass.png" style="max-height:+80px" class="img-fluid d-block mx-auto">
                  <h3 class="mt-3 mb-0">400 R$</h3>
                  <h5 class="font-light m-0">139 บาท</h5>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-6 my-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
            <div class="content-rounded bg-gradient-1 card-hover shadow p-3 mb-5 bg-white rounded">
              <div class="text-center">
                <label for="robux-260" class="w-100 mb-0 py-3 cursor-notallowed content-body bg-gradient-black-1">
                  <input hidden="" type="radio" id="robux-260" name="amount" value="260" data-robux="800" data-amount="260">
                  <img src="https://lawabux.com/assets/images/icon-idpass.png" style="max-height:+80px" class="img-fluid d-block mx-auto">
                  <h3 class="mt-3 mb-0">800 R$</h3>
                  <h5 class="font-light m-0">260 บาท</h5>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-6 my-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="150">
            <div class="content-rounded bg-gradient-1 card-hover shadow p-3 mb-5 bg-white rounded">
              <div class="text-center">
                <label for="robux-1500" class="w-100 mb-0 py-3 cursor-notallowed content-body bg-gradient-black-1">
                  <input hidden="" type="radio" id="robux-1500" name="amount" value="1500" data-robux="22500" data-amount="1500">
                  <img src="https://lawabux.com/assets/images/icon-idpass.png" style="max-height:+80px" class="img-fluid d-block mx-auto">
                  <h3 class="mt-3 mb-0">22,500 R$</h3>
                  <h5 class="font-light m-0">1,500 บาท</h5>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-6 my-3 aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
            <div class="content-rounded bg-gradient-1 card-hover shadow p-3 mb-5 bg-white rounded">
              <div class="text-center">
                <label for="robux-3000" class="w-100 mb-0 py-3 cursor-notallowed content-body bg-gradient-black-1">
                  <input hidden="" type="radio" id="robux-3000" name="amount" value="3000" data-robux="45000" data-amount="3000">
                  <img src="https://lawabux.com/assets/images/icon-idpass.png" style="max-height:+80px" class="img-fluid d-block mx-auto">
                  <h3 class="mt-3 mb-0">45,000 R$</h3>
                  <h5 class="font-light m-0">3,000 บาท</h5>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 row py-5 aos-init" data-aos="fade-up">
      <div class="col-lg-6 mx-auto">
        <div class="text-center bg-dark text-white rounded-lg py-2 callback-content mb-2" style="display:none;">สั่งซื้อด้วย
          <b class="callback-username"></b>จำนวนเงิน
          <b class="callback-amount text-danger"></b>ได้รับ
          <b class="callback-robux text-success"></b>
        </div>
        <a class="btn btn-block btn-danger" href="https://lawabux.com/login?return_url=/robux/idpass">กรุณาเข้าสู่ระบบเพื่อดำเนินการสั่งซื้อ</a>
      </div>
    </div>
  </form>
</div>
