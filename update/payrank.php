        <!-- Payment Rank -->
        <div class="row no-gutters">

          <div class="col-12 col-lg-6 p-2">
          <div class="card shadow-dark radius-border-6 hyper-bg-white text-center p-2 h-100">
          <div class="card-body">
                <h4 class="mt-0 mb-2">การเติมเงินล่าสุด</h4>
                <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ผู้ใช้</th>
                    <th scope="col">จำนวน</th>
                    <th scope="col">เวลา</th>
                  </tr>
                </thead>
                <tbody>
        <?php
          $sql_select_his_pay = "SELECT * FROM history_pay ORDER BY date DESC LIMIT 5";
          $query_his_pay = $hyper->connect->query($sql_select_his_pay);
          $total_his_pay_row = mysqli_num_rows($query_his_pay);
          $his_pay = mysqli_fetch_array($query_his_pay);

          if($total_his_pay_row <= 0){
          ?>
          <h4 class="text-center w-100 mt-4">ไม่มีการเจิมเงินในขณะนี้</h4>
          <?php 
          }else{
          do{
        ?>
            <tr>
              <th scope="row"><?= $his_pay['username']; ?></th>
              <td><?= number_format($his_pay['amount'],0); ?></td>
              <td><?= $his_pay['date']; ?></td>
            </tr>
        <?php }while ($his_pay = mysqli_fetch_array($query_his_pay));} ?>
                </tbody>
                </table>
          </div>
          </div>
          </div>

          <div class="col-12 col-md-6 p-2">
          <div class="card shadow-dark radius-border-6 hyper-bg-white text-center p-2 h-100">
          <div class="card-body">
                <h4 class="mt-0 mb-2">ลำดับการเติมเงินสูงสุด</h4>
                <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ลำดับ</th>
                    <th scope="col">ผู้ใช้</th>
                    <th scope="col">จำนวน</th>
                  </tr>
                </thead>
                <tbody>
        <?php
          $sql_select_his_pay = "SELECT SUM(amount) AS 'totalpay', username FROM history_pay GROUP BY username ORDER BY totalpay DESC LIMIT 5";
          $query_his_pay = $hyper->connect->query($sql_select_his_pay);
          $total_his_pay_row = mysqli_num_rows($query_his_pay);
          $his_pay = mysqli_fetch_array($query_his_pay);

          if($total_his_pay_row <= 0){
          ?>
          <h4 class="text-center w-100 mt-4">ไม่มีการเจิมเงินในขณะนี้</h4>
          <?php 
          }else{
            $count = 1;
          do{
        ?>
                <tr>
                  <th scope="row"><?= $count; ?></th>
                  <td><?= $his_pay['username']; ?></td>
                  <td><?= number_format($his_pay['totalpay'],0); ?></td>
                </tr>
        <?php $count++;}while ($his_pay = mysqli_fetch_array($query_his_pay));} ?>
                </tbody>
                </table>          
          </div>
          </div>
          </div>

        </div>
        <!-- End Payment Rank -->